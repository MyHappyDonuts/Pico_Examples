/*!
 * \file   Pico_Uart.h
 * \brief  Main file
 * \author Javier Morales
 *
 * UART driver
 */

#ifndef PICO_UART_H_
#define PICO_UART_H_

//-----------------------------------------------------------------------------
/* Inclusions */
//-----------------------------------------------------------------------------

/* Standard libraries */

/* Manufacturer libraries */

/* Specific libraries */

//-----------------------------------------------------------------------------
/* Constants */
//-----------------------------------------------------------------------------
#define BUFFER_SIZE			128

//-----------------------------------------------------------------------------
/* Types and structures */
//-----------------------------------------------------------------------------
typedef struct stMessage {
	uint8_t pui8Buffer[BUFFER_SIZE];
	uint8_t ui8Size;
} stMessage;


//-----------------------------------------------------------------------------
/* Imported variables */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Function prototypes */
//-----------------------------------------------------------------------------
void Pico_UartInit(void);
void Pico_UartTx(stMessage* stTxMessage);
stMessage* Pico_UartReadRx(void);
uint8_t* Pico_UartFlagNewMessage(void);

#endif /* PICO_UART_H_ */
