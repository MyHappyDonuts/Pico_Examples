/*!
 * \file   Pico_Uart.c
 * \brief  Main file
 * \author Javier Morales
 *
 * UART driver
 */

//-----------------------------------------------------------------------------
/* Inclusions */
//-----------------------------------------------------------------------------

/* Standard libraries */
#include <string.h>

/* Manufacturer libraries */
#include "hardware/gpio.h"
#include "hardware/uart.h"
#include "hardware/irq.h"

/* Specific libraries */
#include "Pico_Uart.h"

//-----------------------------------------------------------------------------
/* Constants */
//-----------------------------------------------------------------------------
#define UART_ID 		uart0
#define BAUD_RATE 		115200
#define DATA_BITS		8
#define STOP_BITS		1
#define PARITY   		UART_PARITY_NONE

#define UART_TX_PIN 0
#define UART_RX_PIN 1

//-----------------------------------------------------------------------------
/* Types and structures */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Private function prototypes */
//-----------------------------------------------------------------------------
static void Pico_UartRxIrq();

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------
uint8_t Gpui8RxBuffer[128];
uint8_t Gui8Index = 0;

static uint8_t Gui8NewRx;		    /* Received message flag */
static stMessage GstRxMessage;	    /* Received message */


//-----------------------------------------------------------------------------
/* Imported variables */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Functions */
//-----------------------------------------------------------------------------
void Pico_UartInit(void)
{
    /* Received message flag is reset */
	Gui8NewRx = 0;

	/* UART initialization with basic baudrate */
    uart_init(UART_ID, 2400);

    /* Tx and Rx pins configuration */
    gpio_set_function(UART_TX_PIN, GPIO_FUNC_UART);
    gpio_set_function(UART_RX_PIN, GPIO_FUNC_UART);

    /* Baudrate configuration (can also be done with uart_init) */
    uart_set_baudrate(UART_ID, BAUD_RATE);

    /* Control flow is off */
    uart_set_hw_flow(UART_ID, false, false);

    /* UART configuration */
    uart_set_format(UART_ID, DATA_BITS, STOP_BITS, PARITY);

    /* FIFO configuration */
    uart_set_fifo_enabled(UART_ID, true);

    /* Reception interrupt handler set up and enable */
    irq_set_exclusive_handler(UART0_IRQ, Pico_UartRxIrq);
    irq_set_enabled(UART0_IRQ, true);

    /* UART interrupt is configured on reception only */
    uart_set_irq_enables(UART_ID, true, false);
}

void Pico_UartTx(stMessage* stTxMessage)
{
	for (uint8_t ui8Index = 0; ui8Index < stTxMessage->ui8Size; ui8Index++) {
		uart_putc_raw(UART_ID, stTxMessage->pui8Buffer[ui8Index]);
	}
}

static void Pico_UartRxIrq()
{
    uint8_t ui8Index = 0;
    
    while (uart_is_readable(UART_ID)) {
        GstRxMessage.pui8Buffer[ui8Index] = uart_getc(UART_ID);
        ui8Index++;
    }
    /* Number of received bytes */
    GstRxMessage.ui8Size = ui8Index;

    /* Received message flag is set */
    Gui8NewRx = 1;
}

stMessage* Pico_UartReadRx(void)
{
	return &GstRxMessage;
}


uint8_t* Pico_UartFlagNewMessage(void)
{
	return &Gui8NewRx;
}

