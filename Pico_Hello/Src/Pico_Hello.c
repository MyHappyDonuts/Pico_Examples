/*!
 * \file   Pico_Hello.c
 * \brief  Main file
 * \author Javier Morales
 *
 * Hello project using UART. It trasmits a message every second. When it 
 * receives a message, the LED toggles and it sends the message back.
 */

//-----------------------------------------------------------------------------
/* Inclusions */
//-----------------------------------------------------------------------------

/* Standard libraries */
#include <string.h>

/* Manufacturer libraries */
#include "pico/stdlib.h"

/* Specific libraries */
#include "Pico_Hello.h"
#include "Pico_Systick.h"
#include "Pico_Gpio.h"
#include "Pico_Uart.h"

//-----------------------------------------------------------------------------
/* Constants */
//-----------------------------------------------------------------------------
#define ui32PERIOD_HELLO        1000
#define ui32PERIOD_RECEPTION    50

#define sMESSAGE_HELLO			"Hello!\r\n"
//-----------------------------------------------------------------------------
/* Types and structures */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Private function prototypes */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------
static uint32_t Gui32Now;
static uint32_t Gui32MomentHello;
static uint32_t Gui32MomentRx;

static stMessage GstTxMessage;			/* Hello message to be transmitted */
static uint8_t* Gpui8NewMessage;		/* Received message flag */

//-----------------------------------------------------------------------------
/* Imported variables */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Functions */
//-----------------------------------------------------------------------------
void Pico_Hello_Init(void)
{
    Pico_SystickInit();
    Pico_GpioInit();
    Pico_UartInit();   

    /* Hello message struct is initialized */
	GstTxMessage.ui8Size = strlen(sMESSAGE_HELLO);
	memcpy (GstTxMessage.pui8Buffer, sMESSAGE_HELLO, GstTxMessage.ui8Size);

    /* Received message flag is passed from UART driver */
    Gpui8NewMessage = Pico_UartFlagNewMessage();

    Gui32Now = Pico_SystickGet();
    Gui32MomentHello = Gui32Now + ui32PERIOD_HELLO;
    Gui32MomentRx = Gui32Now + ui32PERIOD_RECEPTION;
}

void Pico_Hello_Main(void)
{
    stMessage* stRxMessage;

    Gui32Now = Pico_SystickGet();

    if (Gui32Now >= Gui32MomentHello) {
        Pico_UartTx(&GstTxMessage);
        Gui32MomentHello = Gui32Now + ui32PERIOD_HELLO;
    }

    if (Gui32Now >= Gui32MomentRx) {
        /* Received message flag is detected */
        if (*Gpui8NewMessage) {
            /* The message is read */
            stRxMessage = Pico_UartReadRx();

            /* The message is sent back */
            Pico_UartTx(stRxMessage);

            /* Toggle LED */
            Pico_GpioLedToggle();

            /* The received message flag is cleared */
		    *Gpui8NewMessage = 0;

            Gui32MomentRx = Gui32Now + ui32PERIOD_RECEPTION;
        }
    }
    sleep_ms(1);
}
