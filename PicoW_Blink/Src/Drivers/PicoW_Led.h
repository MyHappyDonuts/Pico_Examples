/*!
* \file Pico_Led.h
* \brief Brief description of the file
* \author Javier Morales
*
* Detailed description of the file
*/

#ifndef _PICO_LED_H        /* Guard against multiple inclusion */
#define _PICO_LED_H

//-----------------------------------------------------------------------------
/* Inclusions */
//-----------------------------------------------------------------------------

/* Standard libraries */

/* Manufacturer libraries */

/* Common libraries */

/* Specific libraries */

//-----------------------------------------------------------------------------
/* Constants definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Types and structs definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Exported function prototypes */
//-----------------------------------------------------------------------------
void PicoW_LedInit(void);
void PicoW_LedOn(void);
void PicoW_LedOff(void);
void PicoW_LedToggle(void);

#endif /* _PICO_LED_H */
//-----------------------------------------------------------------------------
/*!
 End of file
*/
