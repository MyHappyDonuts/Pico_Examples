/*!
* \file Pico_Led.c
* \brief Brief description of the file
* \author Javier Morales
*
* Detailed description of the file
*/

//-----------------------------------------------------------------------------
/* Inclusions */
//-----------------------------------------------------------------------------

/* Standard libraries */

/* Manufacturer libraries */
#include "pico/stdlib.h"


/* Common libraries */

/* Specific libraries */
#include "PicoW_Led.h"

//-----------------------------------------------------------------------------
/* Constants definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Types and structs definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Non-exported function prototypes */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------
uint8_t Gui8LedStatus;
//-----------------------------------------------------------------------------
/* Variables et fonctions importées */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Body functions */
//-----------------------------------------------------------------------------
void PicoW_LedInit(void)
{
    stdio_init_all();
    if (cyw43_arch_init()) {
        printf("WiFi init failed");
    }
    Gui8LedStatus = 0;
}

void PicoW_LedOn(void)
{
    cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);
    Gui8LedStatus = 1;
}

void PicoW_LedOff(void)
{
    cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);
    Gui8LedStatus = 0;
}

void PicoW_LedToggle(void)
{
    if (Gui8LedStatus) {
        cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);
        Gui8LedStatus = 0;
    } else {
        cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);
        Gui8LedStatus = 1;
    } 
}

//-----------------------------------------------------------------------------
/*!
 End of file
*/
