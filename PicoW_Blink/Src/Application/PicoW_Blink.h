/*!
* \file Pico_Blink.h
* \brief Brief description of the file
* \author Javier Morales
*
* Detailed description of the file
*/

#ifndef _PICO_BLINK_H        /* Guard against multiple inclusion */
#define _PICO_BLINK_H

//-----------------------------------------------------------------------------
/* Inclusions */
//-----------------------------------------------------------------------------

/* Standard libraries */

/* Manufacturer libraries */

/* Common libraries */

/* Specific libraries */

//-----------------------------------------------------------------------------
/* Constants definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Types and structs definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Exported function prototypes */
//-----------------------------------------------------------------------------
void Pico_BlinkInit(void);
void Pico_BlinkMain(void);

#endif /* _PICO_BLINK_H */
//-----------------------------------------------------------------------------
/*!
 End of file
*/
