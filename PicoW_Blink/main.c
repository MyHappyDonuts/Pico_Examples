#include "PicoW_Blink.h"
#include "pico/cyw43_arch.h"

int main() {
    PicoW_BlinkInit();
    
    while (1) {
        PicoW_BlinkMain();
    }
}