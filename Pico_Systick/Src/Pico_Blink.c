/*!
 * \file   Pico_Blink.c
 * \brief  Main file
 * \author Javier Morales
 *
 * Blink with Systick example
 */

//-----------------------------------------------------------------------------
/* Inclusions */
//-----------------------------------------------------------------------------

/* Standard libraries */
#include <stdio.h>

/* Manufacturer libraries */
#include "pico/stdlib.h"

/* Specific libraries */
#include "Pico_Blink.h"
#include "Pico_Systick.h"
#include "Pico_Gpio.h"

//-----------------------------------------------------------------------------
/* Constants */
//-----------------------------------------------------------------------------
#define ui32PERIOD_BLINK    500
//-----------------------------------------------------------------------------
/* Types and structures */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Private function prototypes */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------
static uint32_t Gui32Now;
static uint32_t Gui32MomentBlink;

//-----------------------------------------------------------------------------
/* Imported variables */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Functions */
//-----------------------------------------------------------------------------
void Pico_Blink_Init(void)
{
    Pico_SystickInit();
    Pico_GpioInit();

    Gui32Now = Pico_SystickGet();
    Gui32MomentBlink = Gui32Now + ui32PERIOD_BLINK;
}

void Pico_Blink_Main(void)
{
    Gui32Now = Pico_SystickGet();

    if (Gui32Now >= Gui32MomentBlink) {
        Pico_GpioLedToggle();
        Gui32MomentBlink = Gui32Now + ui32PERIOD_BLINK;
    }
    sleep_ms(1);
}
