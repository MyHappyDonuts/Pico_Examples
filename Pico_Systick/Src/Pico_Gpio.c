/*!
 * \file   Pico_Gpio.c
 * \brief  Main file
 * \author Javier Morales
 *
 * GPIO driver
 */

//-----------------------------------------------------------------------------
/* Inclusions */
//-----------------------------------------------------------------------------

/* Standard libraries */

/* Manufacturer libraries */
#include "hardware/gpio.h"

/* Specific libraries */
#include "Pico_Gpio.h"

//-----------------------------------------------------------------------------
/* Constants */
//-----------------------------------------------------------------------------
#define ui8LED              25

//-----------------------------------------------------------------------------
/* Types and structures */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Private function prototypes */
//-----------------------------------------------------------------------------
static void Pico_GpioToggle(uint8_t ui8Gpio);

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Imported variables */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Functions */
//-----------------------------------------------------------------------------
void Pico_GpioInit(void)
{
	/* Initialize LED pin */
    gpio_init(ui8LED);
    gpio_set_dir(ui8LED, GPIO_OUT);
}

void Pico_GpioLedToggle(void)
{
	Pico_GpioToggle(ui8LED);
}

/* Generic GPIO toggle function */
void Pico_GpioToggle(uint8_t ui8Gpio)
{
	uint8_t ui8GpioState = gpio_get(ui8Gpio);

	if (ui8GpioState == 0) {
		gpio_put(ui8Gpio, true);
	}
	else {
		gpio_put(ui8Gpio, false);
	}
}