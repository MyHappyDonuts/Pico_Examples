#include <stdio.h>
#include "pico/stdlib.h"

#include "Pico_Blink.h"

int main() {
    Pico_Blink_Init();

    while (1) {
        Pico_Blink_Main();
    }
}
